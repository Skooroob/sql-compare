﻿using System;
using SQLCompare.Dto;
using SQLCompare.Domain;

namespace SQLCompare
{
    class Program
    {
        static void Main(string[] args)
        {
            var instructions = ArgParser.Interpret(args);
            DbCompare.Scan(instructions);
        }
    }
}
