﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace SQLCompare.Domain
{
    public class SchemaComparison
    {
        private Dto.SQLDatabase _source;
        private Dto.SQLDatabase _target;
        private Dto.Comparison _result;

        public SchemaComparison(Dto.SQLDatabase source, Dto.SQLDatabase target)
        {
            _source = source;
            _target = target;
            _result = new Dto.Comparison();
        }


        public Dto.Comparison Difference()
        {
            foreach (var table in _source.Tables)
                CompareTables(table);

            return _result;
        }

        private void CompareTables(Dto.Table sourceTable)
        {
            if (TargetHasTable(sourceTable, out Dto.Table existingTable))
            {
                var tableDifference = new Dto.Schema.TableDifference();
                tableDifference.ChangeType = Dto.Change.None;
                tableDifference.ObjectType = Dto.SQLObjects.Table;
                tableDifference.Item = sourceTable;
                foreach (var column in sourceTable.Columns)
                {
                    if (TargetTableHasColumn(column, existingTable, out Dto.Column targetColumn))
                    {
                        //TODO
                    }
                    else
                    {
                        var addition = new Dto.SchemaDifference();
                        addition.ChangeType = Dto.Change.Add;
                        addition.ObjectType = Dto.SQLObjects.Column;
                        addition.Item = column;
                        tableDifference.Columns.Add(addition);
                    }
                }
                _result.SchemaChanges.Add(tableDifference);
            }
            else
            {
                var diff = new Dto.SchemaDifference();
                diff.ChangeType = Dto.Change.Add;
                diff.ObjectType = Dto.SQLObjects.Table;
                diff.Item = sourceTable;
                _result.SchemaChanges.Add(diff);
            }
        }

        private bool TargetHasTable(Dto.Table sourceTable, out Dto.Table targetTable)
        {
            targetTable = _target.Tables.FirstOrDefault(t => t.Name == sourceTable.Name);
            return targetTable != null;
        }

        private bool TargetTableHasColumn(Dto.Column sourceColumn, Dto.Table targetTable, out Dto.Column targetColumn)
        {
            targetColumn = targetTable.Columns.FirstOrDefault(c => c.Name == sourceColumn.Name);
            return targetColumn != null;
        }

    }
}
