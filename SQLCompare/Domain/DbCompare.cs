﻿using SQLCompare.Dal;
using SQLCompare.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace SQLCompare.Domain
{
    public class DbCompare
    {
        private UserParams _params;
        private SQLDatabase _source;
        private SQLDatabase _target;
        private Comparison _comparison;

        public DbCompare(UserParams p)
        {
            _params = p;
        }

        public void Scans()
        {
            //1. get data/schema from source (no change) and target (change) databases
            GetData();

            //2. compare the data/schema
            CompareData();

            //3. generate tsql script to update schema, data of target database
            GenerateScript();
        }

        private void GetData()
        {
            var a = new AutoQuery();
            _source = a.GetDatabase(_params.Source, _params.Option);
            _target = a.GetDatabase(_params.Target, _params.Option);
        }

        private void CompareData()
        {
            var sc = new SchemaComparison(_source, _target);
            _comparison = sc.Difference();
        }

        private void GenerateScript()
        {

        }

        public static void Scan(UserParams p)
        {
            var c = new DbCompare(p);
            c.Scans();
        }
    }
}
