﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLCompare.Dto
{
    public class Comparison
    {
        public List<SchemaDifference> SchemaChanges { get; set; }

        public Comparison()
        {
            SchemaChanges = new List<SchemaDifference>();
        }
    }
}
