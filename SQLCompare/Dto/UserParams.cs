﻿namespace SQLCompare.Dto
{
    public class UserParams
    {
        public string Source { get; set; }
        public string Target { get; set; }
        public Options Option { get; set; }
    }

    public enum Options
    {
        SchemaAndData,
        SchemaOnly,
        DataOnly
    }
}
