﻿using System.Collections.Generic;

namespace SQLCompare.Dto.Schema
{
    public class TableDifference : SchemaDifference
    {
        public List<SchemaDifference> Columns { get; set; }

        public TableDifference()
        {
            Columns = new List<SchemaDifference>();
        }
    }
}
