﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLCompare.Dto
{
    public class SchemaDifference
    {
        public Change ChangeType { get; set; }
        public SQLObjects ObjectType { get; set; }
        public object Item { get; set; }
    }


    public enum Change
    {
        Add,
        Remove,
        Edit,
        None
    }

    public enum SQLObjects
    {
        Table,
        Column,
        Relationship,
        PrimaryKey,
        //etc
    }
}
