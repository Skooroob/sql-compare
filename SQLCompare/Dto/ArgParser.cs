﻿using System;

namespace SQLCompare.Dto
{
    public class ArgParser
    {
        private string[] _args;
        private UserParams _result;

        public ArgParser(string[] args)
        {
            _args = args;
        }

        public UserParams Interprets()
        {
            Validate();
            Pack();
            return _result;
        }

        private void Pack()
        {
            _result = new UserParams();
            _result.Source = _args[0];
            _result.Target = _args[1];
            for (int i = 2; i < _args.Length; i++)
                Read(_args[i]);
        }

        private void Read(string arg)
        {
            if (!arg.Contains("-"))
                throw new Exception(Help.NotRecognized(arg));
            var letter = arg.Substring(1, arg.Length-1).ToLower();
            if (letter == "s")
                _result.Option = Options.SchemaOnly;
            else if (letter == "d")
                _result.Option = Options.DataOnly;
            else if (letter == "sd")
                _result.Option = Options.SchemaAndData;
            else
                throw new Exception(Help.NotRecognized(arg));
        }

        private void Validate()
        {
            if (_args == null)
                throw new Exception(Help.Message);
            else if(_args.Length < 2)
                throw new Exception(Help.Message);
            for (int i = 0; i < 2; i++)
                if (string.IsNullOrEmpty(_args[i]))
                    throw new Exception(Help.Message);
        }

        public static UserParams Interpret(string[] args)
        {
            var a = new ArgParser(args);
            return a.Interprets();
        }
    }
}
