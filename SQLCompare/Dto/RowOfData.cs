﻿using System.Collections.Generic;

namespace SQLCompare.Dto
{
    public class RowOfData
    {
        public List<object> Data { get; set; }

        public RowOfData()
        {
            Data = new List<object>();
        }
    }
}
