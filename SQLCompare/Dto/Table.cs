﻿using System.Collections.Generic;

namespace SQLCompare.Dto
{
    public class Table
    {
        public string Name { get; set; }
        public List<Column> Columns { get; set; }
        public List<RowOfData> RawData { get; set; }
        public Table(string name)
        {
            Name = name;
            Columns = new List<Column>();
            RawData = new List<RowOfData>();
        }
    }
}
