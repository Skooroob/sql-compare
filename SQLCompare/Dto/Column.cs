﻿namespace SQLCompare.Dto
{
    public class Column
    {
        public string Name { get; set; }
        public string DataType { get; set; }
        public bool AllowNulls { get; set; }
        public int CharacterMaximumLength { get; set; }
        public int NumericPrecision { get; set; }
        public int DateTimePrecision { get; set; }
    }
}
