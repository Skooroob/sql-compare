﻿using System.Collections.Generic;

namespace SQLCompare.Dto
{
    public class SQLDatabase
    {
        public string ConnectionString { get; set; }
        public string Name { get; set; }
        public List<Table> Tables { get; set; }

        public SQLDatabase(string connectionString)
        {
            ConnectionString = connectionString;
            Tables = new List<Table>();
        }
    }
}
