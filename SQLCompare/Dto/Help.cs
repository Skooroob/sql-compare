﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLCompare.Dto
{
    public class Help
    {
        public static string Message =
@"Enter args in the following format:
-<Source DB connection string> -<Target DB connection string> -<output filename> -<options, such as..>
-d      data only
-s      schema only
-sd     both schema and data

Example:

""Data Source=(local)\SomeServer;Initial Catalog=SourceDatabase;"" ""Data Source=(local)\SomeServer;Initial Catalog=TargetDatabase;"" -s";

        public static string NotRecognized(string arg)
        {
            return $@"Error: '{arg}' is not a valid parameter";
        }
    }
}
