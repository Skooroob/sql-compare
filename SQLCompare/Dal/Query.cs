﻿using SQLCompare.Dto;
using System.Linq;

namespace SQLCompare.Dal
{
    public class Query
    {
        //https://stackoverflow.com/questions/175415/how-do-i-get-list-of-all-tables-in-a-database-using-tsql
        public static string Tables =
@"SELECT TABLE_NAME 
FROM ***.INFORMATION_SCHEMA.TABLES 
WHERE TABLE_TYPE = 'BASE TABLE'";

        //https://stackoverflow.com/questions/1054984/how-can-i-get-column-names-from-a-table-in-sql-server
        public static string Columns =
@"SELECT col.COLUMN_NAME, col.IS_NULLABLE, col.DATA_TYPE, col.CHARACTER_MAXIMUM_LENGTH, col.NUMERIC_PRECISION, col.DATETIME_PRECISION
FROM ***.INFORMATION_SCHEMA.COLUMNS col
WHERE TABLE_NAME = N'&&&'";


        public static string GetTables(string databaseName)
        {
            return Tables.Replace("***", databaseName);
        }

        public static string GetColumns(string databaseName, string tableName)
        {
            return Columns.Replace("***", databaseName).Replace("&&&", tableName);
        }

        public static string GetData(Table table)
        {
            var columnNames = string.Join(", ", table.Columns.Select(c => $"[{c.Name}]"));
            var result = 
$@"SELECT {columnNames}
FROM [{table.Name}]";
            return result;
        }
    }
}
