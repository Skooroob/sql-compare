﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace SQLCompare.Dal
{
    public class AutoQuery
    {
        private Dto.SQLDatabase _database;
        private Dto.Options _option;

        public Dto.SQLDatabase GetDatabase(string connectionString, Dto.Options options)
        {
            _database = new Dto.SQLDatabase(connectionString);
            _option = options;
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                _database.Name = $"[{connection.Database}]";
                GetSchema(connection);
               // GetData(connection);
               //we may have to get the data at a later time due to size limitations
            }
            return _database;
        }

        private void GetSchema(SqlConnection connection)
        {
            var query = Query.GetTables(_database.Name);
            var cmd = new SqlCommand(query, connection);
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    var tableName = reader.GetString(0);
                    _database.Tables.Add(new Dto.Table(tableName));
                }
            }
            foreach (var table in _database.Tables)
            {
                var qu = Query.GetColumns(_database.Name, table.Name);
                var cm = new SqlCommand(qu, connection);
                using (SqlDataReader reader = cm.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var column = new Dto.Column();
                        column.Name = reader.GetString(0);
                        column.AllowNulls = ToBool(reader.GetString(1));
                        column.DataType = reader.GetString(2);
                        var maxLen = SafeGet(reader, 3);
                        if(maxLen != null)
                            column.CharacterMaximumLength = Convert.ToInt32(maxLen.ToString());
                        var numPrec = SafeGet(reader, 4);
                        if(numPrec != null)
                            column.NumericPrecision = Convert.ToInt32(numPrec.ToString());
                        var dtimePrec = SafeGet(reader, 5);
                        if(dtimePrec != null)
                            column.DateTimePrecision = Convert.ToInt32(dtimePrec.ToString());
                        table.Columns.Add(column);
                    }
                }
            }
        }
        
        private void GetData(SqlConnection connection)
        {
            if (_option == Dto.Options.SchemaOnly)
                return;

            foreach (var table in _database.Tables)
            {
                var query = Query.GetData(table);
                var cmd = new SqlCommand(query, connection);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var row = new Dto.RowOfData();
                        for (int i = 0; i < table.Columns.Count; i++)
                            row.Data.Add(SafeGet(reader, i));

                        table.RawData.Add(row);
                    }
                }
            }
        }

        public object SafeGet(SqlDataReader reader, int colIndex)
        {
            if (!reader.IsDBNull(colIndex))
                return reader.GetFieldValue<object>(colIndex);
            return null;
        }

        private bool ToBool(string yesNo)
        {
            var y = yesNo.ToLower();
            if (y == "yes")
                return true;
            else
                return false;
        }
    }
}
